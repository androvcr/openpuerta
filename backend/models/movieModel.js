var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var movieSchema = new Schema({	'name' : String,	'year' : Number,	'duration' : Number});

module.exports = mongoose.model('movie', movieSchema);
