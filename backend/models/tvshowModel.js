var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var tvshowSchema = new Schema({
	'name' : String,
	'chapters' : Number,
	'year' : Number
});

module.exports = mongoose.model('tvshow', tvshowSchema);
