var express = require('express');
var router = express.Router();
var tvshowController = require('../controllers/tvshowController.js');

/*
 * GET
 */
router.get('/', tvshowController.list);

/*
 * GET
 */
router.get('/:id', tvshowController.show);

/*
 * POST
 */
router.post('/', tvshowController.create);

/*
 * PUT
 */
router.put('/:id', tvshowController.update);

/*
 * DELETE
 */
router.delete('/:id', tvshowController.remove);

module.exports = router;
