var tvshowModel = require('../models/tvshowModel.js');

/**
 * tvshowController.js
 *
 * @description :: Server-side logic for managing tvshows.
 */
module.exports = {

    /**
     * tvshowController.list()
     */
    list: function (req, res) {
        tvshowModel.find(function (err, tvshows) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting tvshow.',
                    error: err
                });
            }
            return res.json(tvshows);
        });
    },

    /**
     * tvshowController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        tvshowModel.findOne({_id: id}, function (err, tvshow) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting tvshow.',
                    error: err
                });
            }
            if (!tvshow) {
                return res.status(404).json({
                    message: 'No such tvshow'
                });
            }
            return res.json(tvshow);
        });
    },

    /**
     * tvshowController.create()
     */
    create: function (req, res) {
        var tvshow = new tvshowModel({			name : req.body.name,			chapters : req.body.chapters,			year : req.body.year
        });

        tvshow.save(function (err, tvshow) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating tvshow',
                    error: err
                });
            }
            return res.status(201).json(tvshow);
        });
    },

    /**
     * tvshowController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        tvshowModel.findOne({_id: id}, function (err, tvshow) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting tvshow',
                    error: err
                });
            }
            if (!tvshow) {
                return res.status(404).json({
                    message: 'No such tvshow'
                });
            }

            tvshow.name = req.body.name ? req.body.name : tvshow.name;			tvshow.chapters = req.body.chapters ? req.body.chapters : tvshow.chapters;			tvshow.year = req.body.year ? req.body.year : tvshow.year;			
            tvshow.save(function (err, tvshow) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating tvshow.',
                        error: err
                    });
                }

                return res.json(tvshow);
            });
        });
    },

    /**
     * tvshowController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        tvshowModel.findByIdAndRemove(id, function (err, tvshow) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the tvshow.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
