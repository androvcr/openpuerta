
# installing CURL
sudo apt-get install -y curl 

# installing node
# https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y build-essential
sudo apt-get install -y git

# UPDATING NODE
sudo npm cache clean -f
sudo npm install -g n
sudo n stable

# Installing MongoDB
# https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu precise/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org

# Installing NPM - GRUNT - BOWER
sudo npm install -g npm
sudo npm install -g grunt-cli
sudo npm install -g bower
sudo npm install -g yo
sudo npm i -g npm
sudo npm i -g --save lodash
sudo npm i -g install node-fetch --save


# Installing Backend
# https://www.airpair.com/javascript/complete-expressjs-nodejs-mongodb-crud-skeleton
# https://github.com/DamienP33/express-mongoose-generator
sudo npm install express -g --save
sudo npm install express-generator -g
sudo npm install -g mongoose --save 
sudo npm install -g express-mongoose-generator
sudo npm install body-parser --save
sudo npm install method-override --save