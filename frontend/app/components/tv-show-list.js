angular.
  module('OPFrontend').
  component('tvShowList', {
    templateUrl: 'app/components/tv-show-list.html',
    controllerAs: 'vm',
    controller: 'TvShowListController'
  })
  .controller('TvShowListController', ['$scope', 'ShowService', function (
    $scope,
    ShowService
  ) {

    let vm = this;
    vm.shows= [];
    vm.showTable = false;
    vm.showForm = false;
    vm.formLabel = undefined;

    vm.editingModel = {};

    function _init () {
      vm.showTable = false;
      vm.showForm = false;
      vm.editingModel = {};

      ShowService.getShows().then((response) => {
        vm.shows = response.data;
        vm.showTable = true;
      });
    }

    vm.deleteShow = function deleteShow (show) {
      ShowService.deleteShow(show._id).then(_init);
    };

    vm.editShow = function editShow (show=undefined) {
      if (show === undefined) {
        vm.editingModel = {
          name: '',
          year: 1990,
          chapters: 30
        };
        vm.formLabel = 'Add new TV Show';
      } else {
        angular.copy(show, vm.editingModel);
        vm.formLabel = 'Edit TV Show';
      }
      vm.showForm = true;
    };

    vm.save = function save () {
      ShowService.saveShow(vm.editingModel).then(_init);
    };

    vm.cancel = function cancel () {
      vm.showForm = false;
      vm.editingModel = {};
    };

    _init();

  }]);