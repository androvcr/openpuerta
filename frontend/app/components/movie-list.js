angular.
  module('OPFrontend').
  component('movieList', {
    templateUrl: 'app/components/movie-list.html',
    controllerAs: 'vm',
    controller: 'MovieListController'
  })
  .controller('MovieListController', ['$scope', 'MovieService', function (
    $scope,
    MovieService
  ) {
    // Extending controller
    let vm = this;
    vm.movies= [];
    vm.showTable = false;
    vm.showForm = false;
    vm.formLabel = undefined;
    // Edit element for form
    vm.editingModel = {};

    /**
    * Init function to load or refresh data
    */
    function _init () {
      vm.showTable = false;
      vm.showForm = false;
      vm.editingModel = {};
      // Calling Service -> GET
      MovieService.getMovies().then((response) => {
        vm.movies = response.data;
        vm.showTable = true;
      });
    }
    /**
    * Deletes the selected movie after that the reload function is called
    * TODO: Catch and finally
    */
    vm.deleteMovie = function deleteMovie (movie) {
      MovieService.deleteMovie(movie._id).then(_init);
    };

    /**
    * edits a movie
    * if _id property is detected then it's an edit else creating a new movie
    */
    vm.editMovie = function editMovie (movie=undefined) {
      if (movie === undefined) {  // New movie
        vm.editingModel = {
          name: '',
          year: 1980,
          duration: 90
        };
        vm.formLabel = 'Add new Movie';
      } else {  // Editing a movie
        angular.copy(movie, vm.editingModel);
        vm.formLabel = 'Edit Movie';
      }
      vm.showForm = true;
    };

    // Call the save movie
    vm.save = function save () {
      MovieService.saveMovie(vm.editingModel).then(_init);
    };
    // Cancel changes
    vm.cancel = function cancel () {
      vm.showForm = false;
      vm.editingModel = {};
    };
    
    // Calls the init function
    _init();

  }]);