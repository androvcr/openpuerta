angular.
  module('OPFrontend').
  factory('MovieService', ['$http',
    function($http) {
      let service = {};
      let base = 'http://127.0.0.1:8300/api/movie';

      function _getMovies () {
        return $http.get(base);
      }

      function _deleteMovie (movieId) {
        return $http.delete(`${base}/${movieId}`);
      }

      function _saveMovie (movie) { 
        if (movie._id === undefined) { // New movie
          return $http.post(`${base}`,movie);
        } else {  // editing movie
          return $http.put(`${base}/${movie._id}`,movie);
        }
      };

      service.getMovies = _getMovies;
      service.deleteMovie = _deleteMovie;
      service.saveMovie = _saveMovie;

      return service;
    }]);