angular.
  module('OPFrontend').
  factory('ShowService', ['$http',
    function($http) {
      let service = {};
      let base = 'http://127.0.0.1:8300/api/tvshow';

      function _getShows () {
        return $http.get(base);
      }

      function _deleteShow (showId) {
        return $http.delete(`${base}/${showId}`);
      }

      function _saveShow (show) { 
        if (show._id === undefined) { // New show
          return $http.post(`${base}`,show);
        } else {  // editing show
          return $http.put(`${base}/${show._id}`,show);
        }
      };

      service.getShows = _getShows;
      service.deleteShow = _deleteShow;
      service.saveShow = _saveShow;

      return service;
    }]);